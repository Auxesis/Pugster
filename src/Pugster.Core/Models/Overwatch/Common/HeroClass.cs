﻿namespace Pugster
{
    public enum HeroClass
    {
        Undefined = -1,
        Damage = 0,
        Tank = 1,
        Healer = 2
    }
}
