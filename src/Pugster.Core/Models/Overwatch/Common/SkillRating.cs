﻿namespace Pugster
{
    public enum SkillRating
    {
        Unranked = 0,
        Bronze = 1,
        Silver = 1500,
        Gold = 2000,
        Platinum = 2500,
        Diamond = 3000,
        Master = 3500,
        Grandmaster = 4000
    }
}
