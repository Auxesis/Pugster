﻿using Discord;
using Discord.Commands;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Pugster
{
    [RequireOwner]
    public class OverwatchSecretModule : PugsterModuleBase
    {
        private readonly OverwatchController _overwatch;
        private readonly IConfiguration _config;

        public OverwatchSecretModule(OverwatchController overwatch, IConfiguration config)
        {
            _overwatch = overwatch;
            _config = config;
        }

        [Command("loaddefaultheroes")]
        public async Task LoadDefaultHeroesAsync()
        {
            string fileContents = File.ReadAllText(Path.Combine(AppContext.BaseDirectory, "_default_heroes.json"));
            var defaultHeroes = JsonConvert.DeserializeObject<List<Hero>>(fileContents);

            await _overwatch.CreateHeroesAsync(defaultHeroes.ToArray());
            await ReplyAsync("Done");
        }

        [Command("showhero")]
        public async Task ShowHeroAsync([Remainder]Hero hero)
        {
            var embed = new EmbedBuilder()
                .WithTitle($"{hero.Name} ({hero.Class.ToString()})")
                .WithDescription(hero.Description)
                .WithImageUrl(hero.ImageUrl);
            await ReplyAsync("", embed: embed);
        }
    }
}
